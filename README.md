# What is this

It's a type / variable description mechanism targeted at:

- pure C, with the possibility to be used in C++
- critical embedded systems, with the emphasis on having:
  - all type information (schema) being generated at compile-time
  - all memory usage being known at compile-time

the intention being the creation of flexible yet reliable embedded telemetry applications.

Do note that this is **not** a batteries-included framework.

It generates the schema, but it's up to you to transfer it around.

It's part of a larger telemetry software stack that is currently in development
(will be released when the project reaches maturity), but if you want a more
comprehensive solution and can deal with dynamic allocation, I suggest that
you look at [MPack](https://github.com/ludocode/mpack) or its contenders.

# Dependencies
Developed on a system with GCC 9.2.1 / Clang 9.0.1

Older versions of GCC seem to have issues with the usage of weak attribute.
Compiling with clang is possible, but one has to port the `ctd.lds` linker script.

# Where's all the fun at

The library itself is contained in a single neat file - [ctd.h](ctd.h)
An example source [ctd_test.c](ctd_test.c) showcases all of it current capabilities.

There's a couple scripts in the root of the repo which should get you started:

- [ctd_info.sh](ctd_info.sh) compiles ctd_test.c and extracts type information into ctd.info, which can be parsed by [ctd_info.py](ctd_info.py)
- [ctm_info.sh](ctm_info.sh) is a variation of ctd_info.sh which extract more information for further development purposes.
- [ctd_app.sh](ctd_app.sh) compiles and runs the ctd_test.c

The [./stm32](./stm32) folder contains examples of ctd.h library usage in STM32 MCUs.

# Example run

### Compile & extract type info.
```bash
$ ./ctd_info.sh
```
```
Memory Configuration

Name             Origin             Length             Attributes
*default*        0x0000000000000000 0xffffffffffffffff

Linker script and memory map

LOAD /tmp/ccCnwB4c.o

.ctd            0x0000000000000000      0x4ba
                0x0000000000000000                __ctd_start__ = .
 *(ctd_header)
 ctd_header     0x0000000000000000        0x8 /tmp/ccCnwB4c.o
                0x0000000000000000                __ctd_header__
                0x0000000000000008                __ctd_header_end__ = .
 *(ctd)
 ctd            0x0000000000000008      0x4b2 /tmp/ccCnwB4c.o
                0x0000000000000008                ctd_mybool
                0x0000000000000028                ctd_myfloat
                0x000000000000005b                ctd_mydouble
                0x0000000000000091                ctd_myint8_t
                0x00000000000000b4                ctd_myint16_t
                0x00000000000000da                ctd_myint32_t
                0x0000000000000100                ctd_myuint8_t
                0x0000000000000125                ctd_myuint16_t
                0x000000000000014d                ctd_myuint32_t
                0x0000000000000175                ctd_myenum
                0x00000000000001ad                ctd_myarray
                0x00000000000001d6                ctd_mydynamic_array
                0x0000000000000203                ctd_mystruct
                0x0000000000000449                ctd_mystruct_array
                0x0000000000000480                ctd_mystruct_dynamicarray
                0x00000000000004ba                __ctd_end__ = .
```

### Parse type info.
```bash
$ python ./ctd_info.py ctd.info
```
```
Type definition: mybool
    Bool type
    Attributes
        desc : Lep boolean tip
Type definition: myfloat
    Single precision real
    Attributes
        min : 1.0
        max : 100.0
        desc : Lep float tip
Type definition: mydouble
    Double precision real
    Attributes
        min : -10.0
        max : 20.0
        desc : Lep double tip

etc. etc.
```

### Application that is capable of providing its type info.
```bash
$ ./ctd_app.sh
```
```
-- C Type Descriptors example --

__ctd_start__    = 0x564b7dcd5128 __ctd_end__    = 0x564b7dcd55e2 bytes   = 1210
__ctd_id_start__ = 0x564b7dcd7040 __ctd_id_end__ = 0x564b7dcd70b8 entries = 15
__ctm_start__    = 0x564b7dcd55e8 __ctm_end__    = 0x564b7dcd5660 entries = 15

-- CTM : CTD --
CTM id   1 : CTD bytes  32 data mybool$b@desc$Lep boolean tip$$
CTM id   2 : CTD bytes  51 data myfloat$r@min$1.0$@max$100.0$@desc$Lep float tip$$
CTM id   3 : CTD bytes  54 data mydouble$R@min$-10.0$@max$20.0$@desc$Lep double tip$$
CTM id   4 : CTD bytes  35 data myint8_t$I8$@desc$Lep int8_t tip$$
CTM id   5 : CTD bytes  38 data myint16_t$I16$@desc$Lep int16_t tip$$

etc. etc.
```

# Things to be wary of

Being an attempt to essentially jerry-rig type information into a language
clearly not intended to support it, it does have its drawbacks and gotchas that
you need to be aware of:

- this mechanism makes sense in embedded C / C++, where, in most cases, you
  statically allocate your data at compile time.

  If your data is dynamically allocated, but with fixed-size types, it might be
  possible to still use this method, but you will need to maintain the
  CTD_ID / CTM tables at run-time.

- being a wrapper around C types, you still need to be cautious of the
  data ordering / padding gotchas (which are compiler / target specific).

  Moreover, either the other end that is parsing this data needs to be able to
  parse such gotchas, or the embedded sw developer needs to make sure that these
  issues are not exhibited by properly structuring the data.

- while the CTD Format is simple and generic enough to be applicable for other
  languages, the actual CTD generation will probably have to be implemented in
  some other way

  - Rust has its own macro system - porting to that might be a tour-de-force
  - C++ can generate it via OOP concepts instead
  - Lua / Java / microPython 

In a high-level application setting, it's probably more sensible to
use methods of parameter passing / instrumentation that are provided with
your environment.