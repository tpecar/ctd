////////////////////////////////////////////////////////////////////////////////
// C Type Descriptor (CTD) generation macros
// --------------------------------------------------------------------------
// tpecar, v1.0, 7.4.2020
//
// Provides macros that generate typedefs + compile-time type descriptors.
//
// The source provides data as is + descriptors, and the reader does
// deserialization based on those descriptors.
//
// The idea is based on Efficios Compact Trace Format
// https://diamon.org/ctf/
//
// and follows the Babeltrace2 Trace IR terminology and architecture
// https://babeltrace.org/docs/v2.0/libbabeltrace2/group__api-tir.html
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CTD_H
#define CTD_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
// Most stdbool.h implementations define bool instead of typedefing it,
// and this gives us problems with macro expansion. Let's fix that.
#undef bool
typedef _Bool bool;

////////////////////////////////////////////////////////////////////////////////
// Linker-assembled table constructs
// ---------------------------------
// The idea is that
// - the compiler generates one C Type Descriptor (CTD) symbol per ctd_typedef
//    -> CTDs are variable size and can be large
//       (depending on the complexity of the type described)
//    -> CTDs are initialized at compile time.
// - the linker collects all CTDs and orders them sequentially in memory
//   (bounds available via __ctd_start__ / __ctd_end__ symbols)
//
// - since CTDs are variable size, an additional C Type Map (CTM) symbol
//   is generated for each CTD, which points to the start of the CTD.
//    -> CTMs are fixed size and are small.
//    -> CTMs are intialized at compile time (pointer to CTD)
//    -> The linker orders them sequentially in memory, so they form a table
//       (bounds available via __ctm_start__ / __ctm_end__)
//    -> The index of the table represents the ID of the CTD.
//
// - in order for the code to have access to the ID of a CTD, an additional
//   CTD_ID symbol is generated, to form a separate table
//   (bounds available via __ctd_id_start__ / __ctd_id_end__)
//    -> We **assume** that the compiler uses the same order for both the
//       CTD and CTD_ID tables (this holds for GCC).
//    -> The table is initialized at run-time by simply iterating over
//       the table and assigning the index to the CTD_ID symbol.
//    -> The code will use the initialized CTD_ID to get the id.
//
//  - CTDs, CTMs are expected to reside in flash, while CTD_IDs are expected
//    to reside in RAM
////////////////////////////////////////////////////////////////////////////////

// C Type Descriptor (CTD) Table
////

#define CTD_LINK_SECTION(INPUT_SECTION) \
  __attribute__((weak)) \
  __attribute__((section(INPUT_SECTION))) \
  __attribute__((aligned(8)))

extern                         const char __ctd_start__[];                      // Start of ctd section, aligned, no allocation
CTD_LINK_SECTION("ctd_header") const char __ctd_header__[] = "CTD 1.0";         // Header info
extern                         const char __ctd_header_end__[];                 // End of header info, provided by linker script
extern                         const char __ctd_end__[];                        // End of ctd section, provided by linker script

// CTD creation macro.
// Expects to be initialized by a string describing the type
// (done in ctd_typedef)
#define CTD(type_name) \
  __attribute__((aligned(1))) __attribute__((section("ctd"))) \
  const char ctd_ ## type_name[]


// C Type Map (CTM) Table
////

extern const char * __ctm_start__[];
extern const char * __ctm_end__[];

// CTM creation macro.
// Initializes CTM table.
#define CTM(type_name) \
  __attribute__((section("ctm"))) \
  const char * ctm_ ## type_name = ctd_ ## type_name


// C Type Descriptor ID (CTD_ID) Table
// ID of CTD is computed at run-time.
////

extern size_t __ctd_id_start__[];
extern size_t __ctd_id_end__[];

// CTD_ID creation macro.
// Allocates space for CTD_ID entry.
#define CTD_ID(type_name) \
  __attribute__((section("ctd_id"))) \
  size_t ctd_id_ ## type_name

////////////////////////////////////////////////////////////////////////////////
// Type Descriptor constants
// -------------------------
// Supports a subset of BT2 Field Class types, specified in
// babeltrace-2.0.0/include/babeltrace2/trace-ir/field-class.h
//
// We only support statically allocated data types.
////////////////////////////////////////////////////////////////////////////////

#define CTD_BOOL                    "b"
#define CTD_BIT_ARRAY               "B"
#define CTD_UNSIGNED_INTEGER        "i"
#define CTD_SIGNED_INTEGER          "I"
#define CTD_ENUMERATION             "E"
#define CTD_SINGLE_PRECISION_REAL   "r"
#define CTD_DOUBLE_PRECISION_REAL   "R"
#define CTD_STRUCTURE               "S"
#define CTD_STATIC_ARRAY            "A"
#define CTD_TYPE_REFERENCE          "T"

// Dynamic arrays are essentially static structs with a length field + array.
// Fixed size, however only length elements are transferred during copy/log.
////
#define CTD_DYNAMIC_ARRAY_WITH_LENGTH_FIELD   "D"

// Attributes, which are arbitrary key, value pairs.
////
#define CTD_ATTRIBUTE "@"

////////////////////////////////////////////////////////////////////////////////
// Type Descriptor grammar.
//
// Suitable for a depth-first recursive (LL(1)) parser.
//
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (scalar | enum | struct | array | dynamic_array | type_reference)
//                (ctd_attribute map) * __END__
//
//  scalar:       ctd_bool | integer | bit_array | ctd_real
//
//  map:          [string] __END__ [string] __END__
//
// ---
//  integer:      ctd_integer size
//
//  bit_array:    ctd_bit_array size size
//
//  struct:       ctd_struct td +
//
//  enum:         ctd_enum map +
//
//  array:        ctd_array type size
//
//  size:         [string] __END__
// ---
//  dynamic_array: ctd_dynamic_array ctd_struct [type of len]
//                  ctd_array [type of element] [max size]
// ---
//  type_reference: ctd_type_reference name __END__
//
//  References a previously defined type (created via ctd_typedef).
// ---
////////////////////////////////////////////////////////////////////////////////

// Primitive constructs.
////
#define CTD_END "$"                                                             // grammar: __END__

#define STRING(str) str CTD_END                                                 // grammar: [string]
#define MAP(key, value) STRING(key) STRING(value)                               // grammar: map
#define SIZE(size) STRING(# size)                                               // grammar: size

// Typedef / CTD expansion.
//
// The following behaviour of a standards-compliant C preprocessor are used:
//
//  -> the preprocessor keeps undefined macros + its arguments as-is,
//     but expands them when their name is prefixed to give a defined macro.
//
//     This way, we can provide parametrized macros as arguments to
//     other macros, where the outer macro expands the argument macro
//     by prefixing it.
//
//     By different prefixes we can achieve different expansions of a basename.
//
//  -> the preprocessor (except in traditional mode) disallows macro recursion
//     by blacklisting expanded macros before their expansion,
//     which prevents us from directly supporting nesting of same types.
//
//     If we limit the maximum nesting depth, this can be worked around
//     either by:
//      --> providing separate macros for evaluating a nested type at each
//          possible level - possible but unwieldy
//
//      what we do:
//      --> providing depth-limited recursive capability by defferring macro
//          expansion - that is, completely expanding macro arguments before
//          expanding the macro itself.
//
//          The evaluation of the argument expanded macro block is forced by
//          expanding the block within some other macro
//          (EXPAND_ONCE in our case).
//
//          This works because the macro is blacklisted only in the expansion
//          of its own body and not if it is expanded as a part of
//          some other macro.
//
//          Even though macro arguments can be evaluated before macro expansion
//          the result of macro expansion is not evaluated at its depth.
//          So we nest EXPAND_ONCE to max depth to expand all nested macros.
//
//          The blacklisting functionality does not go into effect in
//          EXPAND_ONCE, because all expansion happens in the argument,
//          not the body of the macro - so, before we expand the macro itself.
//
//  -> since the preprocessor only cares about defines, we can
//     use standard C types and keywords as basenames.
//
// More info on other macro magic:
// https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
// https://stackoverflow.com/questions/12447557/can-we-have-recursive-macros
// https://gcc.gnu.org/onlinedocs/cppinternals/Macro-Expansion.html
////

// Macros for macro deferral
//
// The idea is that the macro we want to defer, is split into
//  -> DEFER([macro name]), which expands into [macro name] EMPTY()
//     EMPTY() gets expanded, but since the [macro name] without arguments
//     is not a valid macro, it remains unexpanded.
//     It requires an additional pass to be evaluated as a macro.
//
//  -> macro arguments, which if they represent a valid define / macro,
//     get expanded in the current expansion
//
// As an example:
// 1. expansion (EXPAND_ONCE)
// 1.1. DEFER(cslist_recursive)()(attr, __VA_ARGS__)
// 1.2. cslist_recursive EMPTY() () (attr, __VA_ARGS__)
// 1.3. cslist_recursice () (attr, [variadic args of current macro exp])
//
// 2. expansion
// 2.1. cslist_recursive() (attr, [variadic args of current macro exp])
// 2.2. cslist (attr, [variadic args of current macro exp])
// 2.3. [expansion of cslist]
//
// If you are unsure how some macros get expanded, use them directly and
// evaluate them in EXPAND_ONCE in steps.
////

#define EMPTY(...)                                                              // Expands into nothing, consumes all arguments
#define DEFER(MACRO_NAME) MACRO_NAME EMPTY()                                    // Expands into [macro name] and EMPTY(), which gets immediately evaluated into nothing
#define EXPAND_ONCE(...) __VA_ARGS__                                            // Expands into [variadic arguments], which evaluates all arguments. Variadic args in order to fully parse expressions that include commas.

// Since EXPAND_ONCE only expands one level of deferred macros, call it
// multiple times
#define EXPAND(...) \
  EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE( \
  EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE( \
  EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE( \
  EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE( \
  EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE(EXPAND_ONCE( \
  __VA_ARGS__ \
  ))))) \
  ))))) \
  ))))) \
  ))))) \
  )))))

// Macros that prefix a comma separated list (variadic arguments)
// TODO: explain how it works
#define cslist(prefix, arg1, ...)   prefix ## _ ## arg1 cslist01(prefix, __VA_ARGS__)
#define cslist01(prefix, arg1, ...) prefix ## _ ## arg1 cslist02(prefix, __VA_ARGS__)
#define cslist02(prefix, arg1, ...) prefix ## _ ## arg1 cslist03(prefix, __VA_ARGS__)
#define cslist03(prefix, arg1, ...) prefix ## _ ## arg1 cslist04(prefix, __VA_ARGS__)
#define cslist04(prefix, arg1, ...) prefix ## _ ## arg1 cslist05(prefix, __VA_ARGS__)
#define cslist05(prefix, arg1, ...) prefix ## _ ## arg1 cslist06(prefix, __VA_ARGS__)
#define cslist06(prefix, arg1, ...) prefix ## _ ## arg1 cslist07(prefix, __VA_ARGS__)
#define cslist07(prefix, arg1, ...) prefix ## _ ## arg1 cslist08(prefix, __VA_ARGS__)
#define cslist08(prefix, arg1, ...) prefix ## _ ## arg1 cslist09(prefix, __VA_ARGS__)
#define cslist09(prefix, arg1, ...) prefix ## _ ## arg1 cslist10(prefix, __VA_ARGS__)
#define cslist10(prefix, arg1, ...) prefix ## _ ## arg1 cslist11(prefix, __VA_ARGS__)
#define cslist11(prefix, arg1, ...) prefix ## _ ## arg1 cslist12(prefix, __VA_ARGS__)
#define cslist12(prefix, arg1, ...) prefix ## _ ## arg1 cslist13(prefix, __VA_ARGS__)
#define cslist13(prefix, arg1, ...) prefix ## _ ## arg1 cslist14(prefix, __VA_ARGS__)
#define cslist14(prefix, arg1, ...) prefix ## _ ## arg1 // discard variadic

// Cleans up empty expansions created by cslist invocation
#define type_
#define ctd_
#define attr_

// Placeholder macro used in deferred expansion
#define cslist_recursive() cslist


#define cslist_type(...) cslist(type, __VA_ARGS__)
#define cslist_ctd(...) cslist(ctd, __VA_ARGS__)
#define cslist_attr(...) cslist(attr, __VA_ARGS__)

// Common attributes
////
#define attr_min(min_value) CTD_ATTRIBUTE MAP("min", # min_value)
#define attr_max(max_value) CTD_ATTRIBUTE MAP("max", # max_value)
#define attr_unit(unit)     CTD_ATTRIBUTE MAP("unit", unit)
#define attr_desc(desc)     CTD_ATTRIBUTE MAP("desc", desc)
#define attr_length

// Typedef - C type definition + CTD creation
////
#define ctd_typedef(type_name, req_type) \
  EXPAND( \
  typedef type_ ## req_type type_name type_postfix_ ## req_type; \
  CTD(type_name) = STRING(# type_name) ctd_ ## req_type; \
  ) \
  CTD_ID(type_name); \
  CTM(type_name) \
  // semicolon @ macro call

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: simple scalars
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (scalar)
//                (ctd_attribute map) * __END__
//  scalar:       ctd_bool | ctd_real
////////////////////////////////////////////////////////////////////////////////

#define type_bool(...) bool
#define type_postfix_bool(...)
#define ctd_bool(...) \
  CTD_BOOL \
  DEFER(cslist_recursive)()(attr, __VA_ARGS__) CTD_END

#define type_float(...) float
#define type_postfix_float(...)
#define ctd_float(...) \
  CTD_SINGLE_PRECISION_REAL \
  DEFER(cslist_recursive)()(attr, __VA_ARGS__) CTD_END

#define type_double(...) double
#define type_postfix_double(...)
#define ctd_double(...) \
  CTD_DOUBLE_PRECISION_REAL \
  DEFER(cslist_recursive)()(attr, __VA_ARGS__) CTD_END

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: integer
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (scalar)
//                (ctd_attribute map) * __END__
//
//  scalar:       integer
// ---
//  integer:      ctd_integer size
//
//  size:         [string] __END__
////////////////////////////////////////////////////////////////////////////////

#define type_intX_t(size) \
  int ## size ## _t

#define ctd_intX_t(size, ...) \
  CTD_SIGNED_INTEGER SIZE(size) \
  DEFER(cslist_recursive)()(attr, __VA_ARGS__) CTD_END


#define type_uintX_t(size) \
  uint ## size ## _t

#define ctd_uintX_t(size, ...) \
  CTD_UNSIGNED_INTEGER SIZE(size) \
  DEFER(cslist_recursive)()(attr, __VA_ARGS__) CTD_END


#define type_int8_t(...) type_intX_t(8)
#define type_postfix_int8_t(...)
#define ctd_int8_t(...) ctd_intX_t(8, __VA_ARGS__)

#define type_int16_t(...) type_intX_t(16)
#define type_postfix_int16_t(...)
#define ctd_int16_t(...) ctd_intX_t(16, __VA_ARGS__)

#define type_int32_t(...) type_intX_t(32)
#define type_postfix_int32_t(...)
#define ctd_int32_t(...) ctd_intX_t(32, __VA_ARGS__)

#define type_uint8_t(...) type_uintX_t(8)
#define type_postfix_uint8_t(...)
#define ctd_uint8_t(...) ctd_uintX_t(8, __VA_ARGS__)

#define type_uint16_t(...) type_uintX_t(16)
#define type_postfix_uint16_t(...)
#define ctd_uint16_t(...) ctd_uintX_t(16, __VA_ARGS__)

#define type_uint32_t(...) type_uintX_t(32)
#define type_postfix_uint32_t(...)
#define ctd_uint32_t(...) ctd_uintX_t(32, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: bit_array
// C allows it only in structs.
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (scalar)
//                (ctd_attribute map) * __END__
//
//  scalar:       bit_array
// ---
//  integer:      ctd_integer size
//
//  bit_array:    ctd_bit_array size size
//
//  size:         [string] __END__
////////////////////////////////////////////////////////////////////////////////

#define type_uintX_t_bit(size) uint ## size ## _t

#define ctd_uintX_t_bit(size, bit_width, ...) \
  CTD_BIT_ARRAY SIZE(size) SIZE(bit_width) \
  DEFER(cslist_recursive)()(attr, __VA_ARGS__) CTD_END

#define type_uint8_t_bit(bit_width, ...) type_uintX_t_bit(8)
#define type_postfix_uint8_t_bit(bit_width, ...) : bit_width
#define ctd_uint8_t_bit(bit_width, ...) ctd_uintX_t_bit(8, bit_width, __VA_ARGS__)

#define type_uint16_t_bit(bit_width, ...) type_uintX_t_bit(16)
#define type_postfix_uint16_t_bit(bit_width, ...) : bit_width
#define ctd_uint16_t_bit(bit_width, ...) ctd_uintX_t_bit(16, bit_width, __VA_ARGS__)

#define type_uint32_t_bit(bit_width, ...) type_uintX_t_bit(32)
#define type_postfix_uint32_t_bit(bit_width, ...) : bit_width
#define ctd_uint32_t_bit(bit_width, ...) ctd_uintX_t_bit(32, bit_width, __VA_ARGS__)

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: enum
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (enum)
//                (ctd_attribute map) * __END__
//
//  map:          [string] __END__ [string] __END__
// ---
//  enum:         ctd_enum map +
//
// Do note that attributes are only per enum type, not per value - we don't
// implement the latter because Babeltrace 2.0.0 API does not provide
// attributes for integer ranges.
////////////////////////////////////////////////////////////////////////////////

#define type_enum_arg_
#define ctd_enum_arg_

#define type_enum_arg_value(key, value) key = value,
#define ctd_enum_arg_value(key, value) MAP(# key, # value)

// Variant of attributes when they are used in enums
// (enum type arguments get enum_arg_ prefix)
////
#define type_enum_arg_desc(...)
#define ctd_enum_arg_desc(desc) attr_desc(desc)

#define type_enum(...) enum {DEFER(cslist_recursive)()(type_enum_arg, __VA_ARGS__)}
#define type_postfix_enum(...)
#define ctd_enum(...) \
  CTD_ENUMERATION DEFER(cslist_recursive)()(ctd_enum_arg, __VA_ARGS__) \
  CTD_END

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: struct
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (struct)
//                (ctd_attribute map) * __END__
// ---
//  struct:       ctd_struct td +
////////////////////////////////////////////////////////////////////////////////

#define type_struct_arg_
#define ctd_struct_arg_

#define type_struct_arg_member(member_name, member_type) \
  type_ ## member_type member_name type_postfix_ ## member_type;

#define ctd_struct_arg_member(member_name, member_type) \
  STRING(# member_name) ctd_ ## member_type

// Variant of attributes when they are used in structs
// (struct type arguments get struct_arg_ prefix)
////
#define type_struct_arg_desc(...)
#define ctd_struct_arg_desc(desc) attr_desc(desc)

#define type_struct(...) struct {DEFER(cslist_recursive)()(type_struct_arg, __VA_ARGS__)}
#define type_postfix_struct(...)
#define ctd_struct(...) \
  CTD_STRUCTURE DEFER(cslist_recursive)()(ctd_struct_arg, __VA_ARGS__) \
  CTD_END

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: array
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (array)
//                (ctd_attribute map) * __END__
// ---
//  array:        ctd_array type size
//
//  size:         [string] __END__
////////////////////////////////////////////////////////////////////////////////

#define type_array(array_type, array_size, ...) \
  type_ ## array_type

#define type_postfix_array(array_type, array_size, ...) \
  [ array_size ]

#define ctd_array(array_type, array_size, ...) \
  CTD_STATIC_ARRAY ctd_ ## array_type SIZE(array_size) \
  DEFER(cslist_recursive)()(ctd_struct_arg, __VA_ARGS__) CTD_END

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: type reference
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (type_reference)
//                (ctd_attribute map) * __END__
// ---
//  type_reference: ctd_type_reference name __END__
////////////////////////////////////////////////////////////////////////////////

#define type_type(type_name, ...) \
  type_name

#define type_postfix_type(...)

#define ctd_type(type_name, ...) \
  CTD_TYPE_REFERENCE STRING(# type_name) \
  DEFER(cslist_recursive)()(ctd_struct_arg, __VA_ARGS__) CTD_END

////////////////////////////////////////////////////////////////////////////////
// BNF subsection: statically allocated array with length field,
//  "dynamic" in the sense that only length amount of members
//  are stored.
//
//  Stream parser should stop parsing after length array members.
// ---
//  td:           name type
//  name:         [string] __END__
//  type:         (dynamic_array)
//                (ctd_attribute map) * __END__
// ---
//  struct:       ctd_struct td +
//  array:        ctd_array type size
// ---
// dynamic_array: ctd_dynamic_array ctd_struct [type of length]
//                  ctd_array [type of element] [max size]
// ---
// This type is composed of other types:
//  struct type(
//    'length' : type of length,
//    'array' : array type(type of element)
//  )
////////////////////////////////////////////////////////////////////////////////

#define type_dynamic_array(array_type, length_type, array_size, ...) \
  type_struct( \
    member(length, length_type), \
    member(array, array(array_type, array_size)) \
  )

#define type_postfix_dynamic_array(...)

#define ctd_dynamic_array(array_type, length_type, array_size, ...) \
  ctd_struct( \
    member(length, length_type), \
    member(array, array(array_type, array_size)) \
  )

#endif // CTD_H
