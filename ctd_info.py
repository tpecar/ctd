#!/usr/bin/env python3
# *_* coding: utf-8 *_*

"""
Example parser of the C Type Descriptor (CTD), version 1.0 Format.
"""

import abc
import argparse


# Implementation based on BNF notation taken from ctf.h, version 1.0
class CTDPrimitiveConverter:
    """
    Primitive handler methods need to be implemented in subclasses.
    """
    __metaclass__ = abc.ABCMeta

    # Are provided with string values retireved by CTDParser / sub-node objects and generate a node object
    # The idea here being that one needs only one core parser for the format, while different primitive parsers
    # generate data structures for a particular use case (dump to text, BT2 event class generation, etc.)
    @abc.abstractmethod
    def td(self, type_name, type_def):
        pass

    @abc.abstractmethod
    def bool(self, attrs):
        pass

    @abc.abstractmethod
    def bit_array(self, field_int_size, field_width, attrs):
        pass

    @abc.abstractmethod
    def unsigned_integer(self, width, attrs):
        pass

    @abc.abstractmethod
    def signed_integer(self, width, attrs):
        pass

    @abc.abstractmethod
    def enumeration(self, enum_value_map, attrs):
        pass

    @abc.abstractmethod
    def single_precision_real(self, attrs):
        pass

    @abc.abstractmethod
    def double_precision_real(self, attrs):
        pass

    @abc.abstractmethod
    def structure(self, struct_member_map, attrs):
        pass

    @abc.abstractmethod
    def static_array(self, element_type, size, attrs):
        pass

    @abc.abstractmethod
    def type_reference(self, ref_type_name, attrs):
        pass

    @abc.abstractmethod
    def dynamic_array(self, length_specifier_type, element_type, max_size, attrs):
        pass

    @abc.abstractmethod
    def attr(self, attribute_name, attribute_value):
        pass


class CTDParser:
    """
    Parser superclass.
    It goes depts-first, calling CTDPrimitiveParser to generate leaves / subnodes and provides them as arguments to
    higher node primitive generators
    """

    class ParseError(Exception):
        def __init__(self, parser, msg):
            self.parser = parser
            self.msg = msg
        def __str__(self):
            return f"[{self.parser.last_read_start} : {self.parser.last_read_end}] {self.msg}"

    def __init__(self, ctd : bytes, pc : CTDPrimitiveConverter):
        self.ctd = ctd
        self.pc = pc

        self.last_read_start = 0    # Start index of last read char/string (inclusive)
        self.last_read_end = 0      # End index of last read char/string (non-inclusive)
        self.last_read = None       # Last read char/string

        # Check header
        header = b"CTD 1.0"
        self.last_read_end = len(header)

        read_header = self.ctd[0 : self.last_read_end]

        if not read_header == header:
            raise CTDParser.ParseError(
                self,
                f"Invalid header, expected \"{str(header, 'ascii')}\", got \"{str(read_header, 'ascii')}\""
            )
        # Skip rest of the header
        self.last_read_end = 8

    def _current_char(self):
        return chr(self.ctd[self.last_read_end])

    def _is_attr(self):
        return self._current_char() == '@'

    def _is_end_of_primitive(self):
        return self._current_char() == '$'

    def _is_end_of_members(self):
        return self._is_attr() or self._is_end_of_primitive()

    def _is_end_of_typedef(self):
        return self._current_char() == '\0'

    def _advance(self):
        self.last_read_end += 1

    def _getchar(self):
        self.last_read_start = self.last_read_end

        c = self.last_read = chr(self.ctd[self.last_read_start])
        self.last_read_end += 1
        return c

    def _getstring(self):
        self.last_read_start = self.last_read_end
        self.last_read_end = self.ctd.index(b'$', self.last_read_start)

        s = self.last_read = str(self.ctd[self.last_read_start : self.last_read_end], encoding='ascii')
        self.last_read_end += 1
        return s

    def _getmap(self):
        return (self._getstring(), self._getstring())

    def _getattr(self):
        attr = []
        while self._is_attr():
            self._advance()
            attr.append(self._getmap())

        return attr

    # Generator interface, where calling next skips over NUL and invokes td on next type in stream.
    def parse(self):
        while self.last_read_end < len(self.ctd):
            typedef = self._td()

            if not self._is_end_of_typedef():
                raise CTDParser.ParseError(self, f"Expected end of typedef (\\0), got '{self._current_char()}'")
            else:
                self._advance()

            yield typedef

    #  td:           name type
    # Parses a single type def.
    def _td(self):
        return self.pc.td(type_name=self._getstring(), type_def=self._type())

    #  type:         (scalar | enum | struct | array | dynamic_array | _type_reference)
    #                (ctd_attribute map) * __END__
    #  scalar:       ctd_bool | integer | bit_array | ctd_real
    #  integer:      ctd_integer size
    #  bit_array:    ctd_bit_array size size
    #  struct:       ctd_struct td +
    #  enum:         ctd_enum map +
    #  array:        ctd_array td size
    #  dynamic_array: ctd_dynamic_array ctd_struct [td of len]
    #  type_reference: ctd_type_reference name __END__
    def _type(self):
        try:
            type = {
                'b' : self._bool,
                'B' : self._bit_array,
                'i' : self._unsigned_integer,
                'I' : self._signed_integer,
                'E' : self._enumeration,
                'r' : self._single_precision_real,
                'R' : self._double_precision_real,
                'S' : self._structure,
                'A' : self._static_array,
                'T' : self._type_reference,
                'D' : self._dynamic_array
            }[self._getchar()]()

            self._advance() # __END__
            return type
        except KeyError:
            raise CTDParser.ParseError(self, f"{self.last_read} does not match any CTD type specifier")

    #  scalar:       ctd_bool | integer | bit_array | ctd_real
    def _bool(self):
        return self.pc.bool(attrs=self._attrs())

    def _bit_array(self):
        return self.pc.bit_array(field_int_size=self._getstring(), field_width=self._getstring(), attrs=self._attrs())

    def _unsigned_integer(self):
        return self.pc.unsigned_integer(width=self._getstring(), attrs=self._attrs())

    def _signed_integer(self):
        return self.pc.signed_integer(width=self._getstring(), attrs=self._attrs())

    def _enumeration(self):
        values = []
        while not self._is_end_of_members():
            values.append(self._getmap())

        return self.pc.enumeration(enum_value_map=values, attrs=self._attrs())

    def _single_precision_real(self):
        return self.pc.single_precision_real(self._attrs())

    def _double_precision_real(self):
        return self.pc.double_precision_real(self._attrs())

    def _structure(self):
        members = []
        while not self._is_end_of_members():
            members.append(self._td())

        return self.pc.structure(struct_member_map=members, attrs=self._attrs())

    def _static_array(self):
        return self.pc.static_array(element_type=self._type(), size=self._getstring(), attrs=self._attrs())

    def _type_reference(self):
        return self.pc.type_reference(ref_type_name=self._getstring(), attrs=self._attrs())

    def _dynamic_array(self):
        return self.pc.dynamic_array(
            length_specifier_type=self._type(), element_type=self._type(),
            max_size=self._getstring(), attrs=self._attrs()
        )

    def _attrs(self):
        attrs = self._getattr()
        return [self.pc.attr(*attr) for attr in attrs]


class CTDPrinter(CTDPrimitiveConverter):

    class Printer:
        def __init__(self, value, sub_sections):
            self.value = value
            self.sub_sections = sub_sections

        def print(self, level=0):
            indent = ' ' * (level * 4)

            print(f"{indent}{self.value}")

            for sub_section in self.sub_sections:
                section_items, section_heading = sub_section
                if section_items and section_heading:
                    print(f"{indent}{section_heading}")
                for value in section_items:
                    value.print(level+1)

    def td(self, type_name, type_def):
        return CTDPrinter.Printer(
            f"Type definition: {type_name}",
            [([type_def], None)]
        )

    def bool(self, attrs):
        return CTDPrinter.Printer(
            f"Bool type",
            [(attrs, "Attributes")]
        )

    def bit_array(self, field_int_size, field_width, attrs):
        return CTDPrinter.Printer(
            f"Bit Array, field base type width: {field_int_size}, bit width: {field_width}",
            [(attrs, "Attributes")]
        )

    def unsigned_integer(self, width, attrs):
        return CTDPrinter.Printer(
            f"Unsigned integer, width: {width}",
            [(attrs, "Attributes")]
        )

    def signed_integer(self, width, attrs):
        return CTDPrinter.Printer(
            f"Signed integer, width: {width}",
            [(attrs, "Attributes")]
        )

    def enumeration(self, enum_value_map, attrs):
        value_printer = [CTDPrinter.Printer(f"{map[0]} : {map[1]}", []) for map in enum_value_map]
        return CTDPrinter.Printer(
            f"Enumeration",
            [(value_printer, "Values"), (attrs, "Attributes")]
        )

    def single_precision_real(self, attrs):
        return CTDPrinter.Printer(
            f"Single precision real",
            [(attrs, "Attributes")]
        )

    def double_precision_real(self, attrs):
        return CTDPrinter.Printer(
            f"Double precision real",
            [(attrs, "Attributes")]
        )

    def structure(self, struct_member_map, attrs):
        return CTDPrinter.Printer(
            f"Structure",
            [(struct_member_map, "Members"), (attrs, "Attributes")]
        )

    def static_array(self, element_type, size, attrs):
        return CTDPrinter.Printer(
            f"Static array of size {size}",
            [([element_type], "Element type"), (attrs, "Attributes")]
        )

    def type_reference(self, ref_type_name, attrs):
        return CTDPrinter.Printer(
            f"Reference to type '{ref_type_name}'",
            [(attrs, "Attributes")]
        )

    def dynamic_array(self, length_specifier_type, element_type, max_size, attrs):
        return CTDPrinter.Printer(
            f"Dynamic array of max size {max_size}",
            [([length_specifier_type], "Length specifier type"), ([element_type], "Element type"), (attrs, "Attributes")]
        )

    def attr(self, attribute_name, attribute_value):
        return CTDPrinter.Printer(
            f"{attribute_name} : {attribute_value}",
            []
        )


def main():
    global ctd_path

    with open(ctd_path, 'rb') as ctd_file:
        ctd = ctd_file.read()

        ctd_parser = CTDParser(ctd, CTDPrinter())
        for typedef in ctd_parser.parse():
            typedef.print()

if __name__ == "__main__":
    global system_plugin_path, plugin_path
    global plugins

    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        "ctd_path", type=str,
        help="Path CTD file to parse"
    )

    # Parse command line and add parsed parameters to globals
    globals().update(vars(parser.parse_args()))

    main()