
#include "ctd.h"

ctd_typedef(mybool, bool(desc("Lep boolean tip")));
ctd_typedef(myfloat, float(min(1.0), max(100.0), desc("Lep float tip")));
ctd_typedef(mydouble, double(min(-10.0), max(20.0), desc("Lep double tip")));
ctd_typedef(myint8_t, int8_t(desc("Lep int8_t tip")));
ctd_typedef(myint16_t, int16_t(desc("Lep int16_t tip")));
ctd_typedef(myint32_t, int32_t(desc("Lep int32_t tip")));
ctd_typedef(myuint8_t, uint8_t(desc("Lep uint8_t tip")));
ctd_typedef(myuint16_t, uint16_t(desc("Lep uint16_t tip")));
ctd_typedef(myuint32_t, uint32_t(desc("Lep uint32_t tip")));

ctd_typedef(
  myenum,
  enum(
    value(state1, 1),
    value(state2, 2),
    value(state3, 3),

    desc("Lep enum tip")
));

ctd_typedef(myarray, array(int8_t(), 10, desc("Lep int8_t array")));

ctd_typedef(mydynamic_array, dynamic_array(uint8_t(), uint8_t(), 10));

ctd_typedef(
  mystruct,
  struct(
    member(mymember1, int8_t(desc("Lep int8_t tip"))),
    member(mymember2, float(min(1.0), max(100.0), desc("Lep float tip"))),
    member(mymember3, bool(desc("Lep boolean tip"))),

    member(mymember4,
      struct(
        member(mymember4_1, int8_t(desc("Lep int8_t tip"))),
        member(mymember4_2,
          struct(
            member(mymember4_2_1, int8_t(desc("Lep int8_t tip"))),
            member(mymember4_2_2, int8_t(desc("Se en lep int8_t tip"))),
            member(mymember4_2_3, int8_t(desc("Pa se en lep int8_t tip"))),
          )
        ),
        member(mymember4_3, double(desc("Lep int8_t tip"))),
      )
    ),

    member(mymember5, int8_t()),

    member(mymember6, array(
      int8_t(desc("int8_t array element"), min(10), max(50)),
      10
    )),

    member(mymember7,
      enum(
        value(state21, 1),
        value(state22, 2),
        value(state23, 3),

        desc("Lep enum tip")
      )
    ),

    member(member8,
      dynamic_array(
        dynamic_array(uint16_t(), uint8_t(), 10, desc("Inner dynamic array")),
        uint8_t(), 20, desc("Outer dynamic array")
      )
    ),

    desc("Lep struct tip")
));

ctd_typedef(mystruct_array,
  array(type(mystruct), 10, desc("Lep struct array"))
);

ctd_typedef(mystruct_dynamicarray,
  dynamic_array(type(mystruct), uint8_t(), 10, desc("Lep dynamic struct array"))
);

#include "stdio.h"

int main (){

  puts("-- C Type Descriptors example --\n");

  const size_t ctd_size     = __ctd_end__     - __ctd_start__;
  const size_t ctd_id_size  = __ctd_id_end__  - __ctd_id_start__;
  const size_t ctm_size     = __ctm_end__     - __ctm_start__;

  printf(
    "__ctd_start__    = 0x%zx __ctd_end__    = 0x%zx bytes   = %zu\n"
    "__ctd_id_start__ = 0x%zx __ctd_id_end__ = 0x%zx entries = %zu\n"
    "__ctm_start__    = 0x%zx __ctm_end__    = 0x%zx entries = %zu\n"
    "\n",
    
    (size_t)__ctd_start__,    (size_t)__ctd_end__,    ctd_size,
    (size_t)__ctd_id_start__, (size_t)__ctd_id_end__, ctd_id_size,
    (size_t)__ctm_start__,    (size_t)__ctm_end__,    ctm_size,
    
    ctm_size
  );

  // Initialize CTD ID table
  size_t * ctd_id = __ctd_id_start__;
  for (size_t idx = 0; idx < ctd_id_size; idx++)
    __ctd_id_start__[idx] = idx + 1;

  // Dump CTM + CTD information
  puts("-- CTM : CTD --");

  const char ** ctm = __ctm_start__;
  for (size_t idx = 0; idx < ctm_size; idx++) {
    // Get size of CTD info
    size_t cur_ctd_size =
      idx + 1 < ctm_size
        ? ctm[idx + 1] - ctm[idx]
        : __ctd_end__ - ctm[idx];
    
    printf(
      "CTM id %3zu : CTD bytes %3zu data %.*s\n",
      ctd_id[idx], cur_ctd_size,
      cur_ctd_size, ctm[idx]
    );
  }

  return 0;
}
