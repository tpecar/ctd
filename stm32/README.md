# Initial CTD on stm32 mcu example

Identical to the ctd_test.c example.

![Example running on NUCELO-G431KB](.readme/example-run.png)

## Tools

[STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html), version >= 1.2.0

## Setup
Common to all projects in this folder.

Recreate the project on your own system by using \
**File** > **New** > **STM32 Project From STM32CubeMX .ioc File**.

1. **STM32CubeMX .ioc File**: *.ioc provided in the repository*
2. **Use default location**: *unchecked*, **specify** *location of this repository*
3. Select **Next** to get to the **Firmware Library Package Setup** page
4. **Code Generator Options**: select *Add necessary library files as reference...*

This will pull & install all required dependencies from ST and link them with the project, while keeping them out of the repository.

Any other method will either bloat the repository and/or give you headaches.

## Debug

To get debug output (you need to have a SWO capable target + debugger)

1. Set up Serial Wire Viewer
   ![SWV configuration](.readme/swo-config.png)
   _Note that you have to manually configure your core clock_
2. Include [swo.c](./nucleo-g431kb/Core/Src/swo.c) in your project
3. In the IDE, open the SWV ITM Data Console window
   ![SWV ITM Data Console window](.readme/swv-window.png)
4. **During a running debug session**, configure the SWV ITM Data Console windows as follows
   ![SWV ITM Data Console window setup](.readme/swv-window-config.png)

## Creating a new project

1. Generate a STM32CubeIDE C/C++ project for the board of your choice.
2. Set up the path to the ctd.h header file
   ![ctd.h header file setup](.readme/path-config.png)
3. Adapt the linker script of your project to include CTD / CTD_ID / CTM section set-up.
   Refer to [./nucleo-g431kb/STM32G431KBTX_FLASH.ld](./nucleo-g431kb/STM32G431KBTX_FLASH.ld) for details.