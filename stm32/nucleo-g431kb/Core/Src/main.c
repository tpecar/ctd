/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdio.h>

// C Type Definitions header file
#include "ctd.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

// C Type Descriptors test types

ctd_typedef(mybool, bool(desc("Lep boolean tip")));
ctd_typedef(myfloat, float(min(1.0), max(100.0), desc("Lep float tip")));
ctd_typedef(mydouble, double(min(-10.0), max(20.0), desc("Lep double tip")));
ctd_typedef(myint8_t, int8_t(desc("Lep int8_t tip")));
ctd_typedef(myint16_t, int16_t(desc("Lep int16_t tip")));
ctd_typedef(myint32_t, int32_t(desc("Lep int32_t tip")));
ctd_typedef(myuint8_t, uint8_t(desc("Lep uint8_t tip")));
ctd_typedef(myuint16_t, uint16_t(desc("Lep uint16_t tip")));
ctd_typedef(myuint32_t, uint32_t(desc("Lep uint32_t tip")));

ctd_typedef(
  myenum,
  enum(
    value(state1, 1),
    value(state2, 2),
    value(state3, 3),

    desc("Lep enum tip")
));

ctd_typedef(myarray, array(int8_t(), 10, desc("Lep int8_t array")));

ctd_typedef(mydynamic_array, dynamic_array(uint8_t(), uint8_t(), 10));

ctd_typedef(
  mystruct,
  struct(
    member(mymember1, int8_t(desc("Lep int8_t tip"))),
    member(mymember2, float(min(1.0), max(100.0), desc("Lep float tip"))),
    member(mymember3, bool(desc("Lep boolean tip"))),

    member(mymember4,
      struct(
        member(mymember4_1, int8_t(desc("Lep int8_t tip"))),
        member(mymember4_2,
          struct(
            member(mymember4_2_1, int8_t(desc("Lep int8_t tip"))),
            member(mymember4_2_2, int8_t(desc("Se en lep int8_t tip"))),
            member(mymember4_2_3, int8_t(desc("Pa se en lep int8_t tip"))),
          )
        ),
        member(mymember4_3, double(desc("Lep int8_t tip"))),
      )
    ),

    member(mymember5, int8_t()),

    member(mymember6, array(
      int8_t(desc("int8_t array element"), min(10), max(50)),
      10
    )),

    member(mymember7,
      enum(
        value(state21, 1),
        value(state22, 2),
        value(state23, 3),

        desc("Lep enum tip")
      )
    ),

    member(member8,
      dynamic_array(
        dynamic_array(uint16_t(), uint8_t(), 10, desc("Inner dynamic array")),
        uint8_t(), 20, desc("Outer dynamic array")
      )
    ),

    desc("Lep struct tip")
));

ctd_typedef(mystruct_array,
  array(type(mystruct), 10, desc("Lep struct array"))
);

ctd_typedef(mystruct_dynamicarray,
  dynamic_array(type(mystruct), uint8_t(), 10, desc("Lep dynamic struct array"))
);


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  HAL_Delay(500);

  // Dump CTD info to ITM Port 0 (set up by swo.c)

  puts("-- C Type Descriptors example --\n");

  const size_t ctd_size     = __ctd_end__     - __ctd_start__;
  const size_t ctd_id_size  = __ctd_id_end__  - __ctd_id_start__;
  const size_t ctm_size     = __ctm_end__     - __ctm_start__;

  printf(
    "__ctd_start__    = 0x%08x __ctd_end__    = 0x%08x bytes   = %u\n"
    "__ctd_id_start__ = 0x%08x __ctd_id_end__ = 0x%08x entries = %u\n"
    "__ctm_start__    = 0x%08x __ctm_end__    = 0x%08x entries = %u\n"
    "\n",

    (int)__ctd_start__,    (int)__ctd_end__,    ctd_size,
    (int)__ctd_id_start__, (int)__ctd_id_end__, ctd_id_size,
    (int)__ctm_start__,    (int)__ctm_end__,    ctm_size
  );

  // Initialize CTD ID table
  size_t * ctd_id = __ctd_id_start__;
  for (size_t idx = 0; idx < ctd_id_size; idx++)
    __ctd_id_start__[idx] = idx + 1;

  // Dump CTM + CTD information
  puts("-- CTM : CTD --");

  const char ** ctm = __ctm_start__;
  for (size_t idx = 0; idx < ctm_size; idx++) {
    // Get size of CTD info
    size_t cur_ctd_size =
      idx + 1 < ctm_size
        ? ctm[idx + 1] - ctm[idx]
        : __ctd_end__ - ctm[idx];

    printf(
      "CTM id %3u : CTD bytes %3u data %.*s\n",
      ctd_id[idx], cur_ctd_size,
      cur_ctd_size, ctm[idx]
    );
  }

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage 
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 85;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_8) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks 
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
