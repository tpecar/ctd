/*
 * swo.c
 *
 * SWO based printf.
 *
 * More info:
 * http://blog.atollic.com/cortex-m-debugging-introduction-to-serial-wire-viewer-swv-event-and-data-tracing
 * http://blog.atollic.com/cortex-m-debugging-printf-redirection-to-a-debugger-console-using-swv/itm-part-1
 * http://blog.atollic.com/cortex-m-debugging-printf-redirection-to-a-debugger-console-using-swv/itm-part-2
 *
 * https://ralimtek.com/stm32_debugging/
 * https://empa.com/dokumanlar/st2016/10_Complementary-debug-tools-printf.pdf
 *
 */

#include "main.h"

/*
 * Linker-level override for _write function.
 */
int _write(int file, char *ptr, int len) {
  for (int i = 0; i < len; i++)
    ITM_SendChar((*ptr++));
  return len;
}
